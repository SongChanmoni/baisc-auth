package com.example.simplesecurity.config;

import com.example.simplesecurity.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig {

    @Autowired
    private UserDetailService userDetailService;

    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    //ignore file
    @Configuration
    public static class AnonymousConfigurationAdapter extends WebSecurityConfigurerAdapter{
        private String[] url= {
                "/css/**",
                "/js/**",
                "/image/**",
                "*.html"
        };

        @Override
        public void configure(WebSecurity web) throws Exception {
           web.ignoring().antMatchers(url);
        }
    }


    @Configuration
    @Order(2)
    public static class UserApiConfig extends WebSecurityConfigurerAdapter{

        @Autowired
        private UserDetailService userDetailService;

        @Autowired
        private PasswordEncoder passwordEncoder;


        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
           auth.userDetailsService(userDetailService);
//           auth.userDetailsService(userDetailService).passwordEncoder(passwordEncoder);
        }


        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable().requestMatchers()
                    .antMatchers("/api/**")
                    .and()
                    .authorizeRequests()
                    .antMatchers("/api/v1/login").permitAll()
                    .antMatchers("/api/user").hasAnyRole("USER","ADMIN")
                    .antMatchers("/api/admin").hasRole("ADMIN")
                    .anyRequest()
                    .authenticated()
                    .and()
                    .exceptionHandling();

        }
    }



    @Configuration
    @Order(3)
    public static class WebConfig extends WebSecurityConfigurerAdapter{

        @Autowired
        private UserDetailService userService;

        @Autowired
        private PasswordEncoder passwordEncoder;

        @Override
        protected void configure(AuthenticationManagerBuilder auth) throws Exception {
            auth.userDetailsService(userService);
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {

            http
                    .authorizeRequests()
                    .antMatchers("/admin/**").hasRole("ADMIN")
                    .anyRequest()
                    .authenticated()
                    .and()
                    .formLogin()
                    .defaultSuccessUrl("/",true)
                    .failureUrl("/login?error=true")
                    .permitAll()
                    .and()
                    .csrf()
                    .disable()
                    .logout()
                    .logoutUrl("/logout")
                    .permitAll();
        }
    }


}
