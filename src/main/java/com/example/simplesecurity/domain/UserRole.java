package com.example.simplesecurity.domain;

import com.fasterxml.jackson.annotation.JsonValue;

public enum UserRole {
    ADMIN("ADMIN"),
    USER("USER");

    private final String value;

    UserRole(String value) {this.value = value;}
    @JsonValue
    public String getValue() {
        return value;
    }
}
